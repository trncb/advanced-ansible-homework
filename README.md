# README #

### What is this repository for? ###

Homework assignment for the Advanced Ansible Deployment course

### Prepare Environment 

#### Lab environments
Ansible Tower and OpenStack labs were provisioned during the training course.

#### Tower preparation
* Ansible Tower was configured during the training course.
* Machine credentials for the isolated node in OpenStack were added to Tower.
* Read-only AWS credentials were added to Tower by the trainer.

#### AWS preparation 
* opentlc credentials were placed in a file on the OpenStack workstation and are sourced from there for AWS provisioning.

### Running of playbooks

Playbooks are run from Ansible Tower using the following sequence within a workflow template:

1. Provision infrastructure  
  a. playbook: aws-provision.yml => runs in parallel to OpenStack (2, 3, 4)  
    i) After dynamic inventory sync, check AWS instance status => aws-status-check.yml  
    ii) Copy ssh keys to new instances => aws-ssh-creds.yml  
  b. playbook: site-osp-infrastructure.yml  
2. Create OpenStack instances
  playbook: site-osp-instances.yml  
3. Deploy 3-tier to OpenStack
  playbook: site-3tier-deploy.yml  
4. Smoke-test 3-tier on OpenStack
  playbook: site-smoke-test-osp.yml  
  a. If failure => playbook: site-osp-delete-instances.yml  
  b. If successful => Go to 5)  
5. Deploy 3-tier on AWS
  playbook: site-3tier-deploy.yml  
6. Smoke-test 3-tier on AWS
  playbook: site-smoke-test-aws.yml  
